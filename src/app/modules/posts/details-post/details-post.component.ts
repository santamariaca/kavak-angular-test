import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from 'src/app/shared/services/posts.service';

@Component({
  selector: 'app-details-post',
  templateUrl: './details-post.component.html',
  styleUrls: ['./details-post.component.css']
})
export class DetailsPostComponent implements OnInit {

  post: any;

  constructor(
    private _postService: PostsService,
    private _route: ActivatedRoute
  ) {
  }

  ngOnInit() {
    this.getPostById(this._route.snapshot.params["id"]);
  }

  getPostById(id) {
    this._postService.getPostById(id).subscribe(data => {
      this.post = data;
    }, err => {
      console.error("Error en obtener post por id", err);
    })
  }

}
