import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListPostsComponent } from './list-posts/list-posts.component';
import { DetailsPostComponent } from './details-post/details-post.component';

const routes: Routes = [
  {
    path: '',
    component: ListPostsComponent
  }, {
    path: ':id',
    component: DetailsPostComponent
  }, {
    path: 'busqueda/:palabra',
    component: ListPostsComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PostsRoutingModule { }
