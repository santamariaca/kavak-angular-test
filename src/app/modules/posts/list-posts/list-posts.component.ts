import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { PostsService } from 'src/app/shared/services/posts.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-list-posts',
  templateUrl: './list-posts.component.html',
  styleUrls: ['./list-posts.component.css']
})
export class ListPostsComponent implements OnInit {
  postList: Observable<any>;

  constructor(
    private _postService: PostsService,
    private _route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.getPosts();
  }

  getPosts() {
    this.postList = this._postService.getPosts().pipe(
      map(posts => {
        const filterString = this._route.snapshot.params["palabra"] ? this._route.snapshot.params["palabra"].toLocaleLowerCase() : false;

        if (!filterString) {
          return posts;
        }

        return posts.filter(post => this.filter(filterString, [post.title, post.body]));
      }),
    );
  }

  filter(filter: string, value: Array<string>): boolean {
    const exp = new RegExp(filter, 'gi');
    if (value && value.length > 0)
      return value.filter(v => typeof v == "string" ? exp.test(v.toLocaleLowerCase()) : false).length > 0;
    return false;
  }

}
