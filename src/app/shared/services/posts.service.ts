import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostsService {

  constructor(
    private _http: HttpClient
  ) { }

  getPosts(): Observable<any> {
    return this._http.get(`https://jsonplaceholder.typicode.com/posts`).pipe(
      map((res: Array<any>) => {
        res = res.map(post => {
          post.title = this.capitalizeTitle(post.title)
          return post;
        })
        return res;
      })
    )
  }

  getPostById(id): Observable<any> {
    return this._http.get(`https://jsonplaceholder.typicode.com/posts/${id}`).pipe(
      map((res: any) => {
        res.title = this.capitalizeTitle(res.title)
        return res;
      })
    )
  }


  private capitalizeTitle(title) {
    return title.toLocaleLowerCase().split(" ")
      .map((word: string) => word.charAt(0).toUpperCase() + word.slice(1)).join(" ");
  }

}
